#include <stdio.h>
#include <stdlib.h>

int main(){
    int pid;
    printf( "1) prima della fork \n" );
    pid = fork();
    printf( "2) dopo della fork \n" );
    if (pid == 0){
        printf( "3) sono il processo figlio con pid:%d.", getpid());
        sleep(3);
        printf( "Il mio papi ha pid: %d\n", getppid());
        exit(1);
    }
    else{
        printf( "3) sono il processo padre con pid:%d.", getpid());
        printf( " Il mio papi ha pid: %d\n", getppid());
        exit(0);
    }
}
